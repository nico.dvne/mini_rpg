﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    public abstract class Meuble
    {
        protected int Largeur { get; set; }
        protected int Longueur { get; set; }
        protected int Hauteur { get; set; }

        public abstract void NoticeDeMontage();

        public Meuble(int v_largeur, int v_longueur, int v_hauteur)
        {
            this.Largeur = v_largeur;
            this.Longueur = v_longueur;
            this.Hauteur = v_hauteur;
        }

        public override string ToString()
        {
            return $"Type : {this.GetType().Name} , Longueur :  {Longueur} , Largeur : {Largeur}, Hauteur : {Hauteur}";
        }

    }
}
