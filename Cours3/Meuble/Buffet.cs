﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Buffet : Meuble
    {
        public Buffet(int v_largeur, int v_longueur, int v_hauteur) : base(v_largeur, v_longueur, v_hauteur)
        {

        }
        public override void NoticeDeMontage()
        {
            Console.WriteLine("Notice d'un buffet");
        }
    }
}
