﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Ikea
    {
        private string Secteur { get; }

        private List<Meuble> Catalogue { get; set; }

        public Ikea(string v_secteur, List<Meuble> v_catalogue)
        {
            this.Secteur = v_secteur;
            this.Catalogue = v_catalogue;
        }

        public void ListerLesMeubles()
        {
            foreach(Meuble meuble in Catalogue)
            {
                Console.WriteLine(meuble);
            }
        }

    }
}
