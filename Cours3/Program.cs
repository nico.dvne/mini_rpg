﻿using System;
using System.Collections.Generic;

namespace Cours3
{
    class Program
    {
        static void Main(string[] args)
        {
            int partial_score = 0;
            Console.WriteLine("LE HEROS ENTRE DANS LE DONJON");
            Heros heros = new Heros(50, 75);
            Random random = new Random();
            do
            {
                Monstre monstre = GestionDePartie.GenerateMonster(random.Next(10, 100), random.Next(10, 100));
                Console.WriteLine("UN {0} APPARAIT AVEC {1} PTS D'ATTAQUE ET {2} PV ",monstre,monstre.Attack,monstre.PV);
                do
                {
                    heros.Attaquer(monstre);
                    Console.WriteLine("ATTAQUE DU HEROS");
                    if (monstre.isAlive)
                    {
                        Console.WriteLine("IL RESTE {0} pts de vie au monstre", monstre.PV);
                        monstre.Attaquer(heros);
                        Console.WriteLine("IL RESTE {0} PTS DE VIE AU HEROS", heros.PV);
                    }                    
                } while (heros.isAlive && monstre.isAlive);
                if (heros.isAlive)
                {
                    heros.Score += 1;
                    Console.WriteLine("LE MONSTRE EST MORT");
                }
                if(5 == heros.Score)
                {
                    heros = GestionDePartie.Evolution(heros);
                    Console.WriteLine("LE HEROS EST MAINTENANT UN {0}", heros);
                }
            } while (heros.isAlive);
            Console.WriteLine("");
            Console.WriteLine("LE HEROS A TUE {0} MONSTRES ! ", heros.Score);
        }
    }
}
