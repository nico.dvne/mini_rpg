﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3.Recompense
{
    interface IRecompense
    {
        public static void Recompenser(AbstractPersonnage personnage);
    }
}
