﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3.Recompense
{
    class Tresor : IRecompense
    {
        public static void Recompenser(AbstractPersonnage personnage)
        {
            personnage.BonusPV(50);
        }
    }
}
