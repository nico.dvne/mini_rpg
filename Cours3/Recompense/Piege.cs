﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3.Recompense
{
    class Piege : IRecompense
    {
        public static void Recompenser(AbstractPersonnage personnage)
        {
            personnage.MalusPV((int)personnage.PV % 10);
        }
    }
}
