﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    abstract class AbstractPersonnage 
    {
        public bool isAlive { get; protected set; }
        public int Attack { get; protected set; }
        public int PV { get; protected set; }


        public AbstractPersonnage(int v_attack, int v_pv)
        {
            this.isAlive = true;
            this.Attack = v_attack;
            this.PV = v_pv;
        }

        public void Attaquer(AbstractPersonnage v_opponent)
        {
            v_opponent.Defense(this.Attack);
        }

        public void Defense(int v_opponentAttackValue)
        {
            if (this.PV > v_opponentAttackValue)
                this.PV -= v_opponentAttackValue;
            else
            {
                this.PV = 0;
                this.isAlive = false;
            }
                
        }

        public void BonusPV(int add_pv)
        {
            if (add_pv > 0)
                this.PV += add_pv;
        }

        public void MalusPV(int less_pv)
        {
            if(less_pv > 0 && (this.PV - less_pv > 0))
            {
                this.PV -= less_pv;
            }
        }
    }
}
