﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Heros : AbstractPersonnage
    {
        public int Score { get; set; }
        public Heros(int v_attack, int v_pv) : base(v_attack, v_pv)
        {
            this.Score = 0;
        }
       
    }
}
