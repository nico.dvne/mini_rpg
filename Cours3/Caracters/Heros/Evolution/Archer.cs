﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Archer : Heros
    {
        public Archer(Heros heros) : base(heros.Attack, heros.PV)
        {
            this.Score = heros.Score;
            this.Attack = this.Attack += Convert.ToInt32(this.Attack * 0.15);
            this.PV = this.PV += Convert.ToInt32(this.PV * 0.15);
        }

        public override string ToString()
        {
            return "ARCHER";
        }
    }
}
