﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Chevalier : Heros
    {
        public Chevalier(Heros heros) : base(heros.Attack, heros.PV)
        {
            this.Score = heros.Score;
            this.Attack = this.Attack += Convert.ToInt32(this.Attack * 0.1);
            this.PV = this.PV += Convert.ToInt32(this.PV * 0.2);
        }
        public override string ToString()
        {
            return "CHEVALIER";
        }
    }
}
