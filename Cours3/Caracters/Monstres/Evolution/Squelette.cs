﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Squelette : Monstre
    {
        public Squelette(int v_attack, int v_pv) : base(v_attack % 5, v_pv + 30)
        {
            
        }

        public override string ToString()
        {
            return "SQUELETTE";
        }
    }
}
