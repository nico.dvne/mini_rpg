﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Sorciere : Monstre
    {
        public Sorciere(int v_attack, int v_pv) : base(v_attack + 50, v_pv % 3)
        {
            
        }

        public override string ToString()
        {
            return "SORCIERE";
        }
    }
}
