﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cours3
{
    class Gobelin : Monstre
    {
        public Gobelin(int v_attack, int v_pv) : base(v_attack + 10, v_pv % 10)
        {
           
        }

        public override string ToString()
        {
            return "GOBELIN";
        }
    }
}
