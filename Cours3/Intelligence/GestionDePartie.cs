﻿using Cours3.Recompense;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Cours3
{
    class GestionDePartie
    {
        public static Monstre GenerateMonster(int v_attack, int v_pv)
        {
            Random random = new Random();
            Monstre monstre;
            switch (random.Next(3))
            {
                case 0:
                    monstre = new Gobelin(v_attack, v_pv);
                    break;

                case 1:
                    monstre = new Squelette(v_attack, v_pv);
                    break;

                case 2:
                    monstre = new Sorciere(v_attack, v_pv);
                    break;

                default:
                    monstre = null;
                    break;
            }

            return monstre;
        }

        public static Heros Evolution(Heros heros)
        {
            Random random = new Random();
            
            switch (random.Next(3))
            {
                case 0:
                    heros = new Chevalier(heros);
                    break;

                case 1:
                    heros = new Archer(heros);
                    break;

                case 2:
                    heros = new Magicien(heros);
                    break;

                default:
                    heros = null;
                    break;
            }

            return heros;
        }

        public static void PiegeOuTresor(Heros heros)
        {
            Random random = new Random();
            if (1 == random.Next(0, 2))
                Piege.Recompenser(heros);
            else
            {
                Tresor.Recompenser(heros);
            }
        }
    }
}
